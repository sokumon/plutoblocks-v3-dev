// these are the starter blocks that are always there in the workspace
Blockly.defineBlocksWithJsonArray([{
    "type": "pluto_on_start_loop",
    "message0": "on start %1 %2",

    "args0": [
      {
        "type": "input_dummy",
        "align": "RIGHT"
      },
      {
        "type": "input_statement",
        "name": "NAME",
        "align": "CENTRE"
      }
    ],
    "colour": 300,
    "tooltip": "",
    "helpUrl": "https://create.dronaaviation.com/assets/downloads/PlutoBlocks/Guide%20to%20Pluto%20Blocks%20updated.pdf"
  }]);


Blockly.Blocks['pluto_loop'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("pluto loop");
          this.setDeletable(false);
      this.appendStatementInput("name")
          .setCheck(null);
      this.appendDummyInput()
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("LoopTime(ms)")
          .appendField(new Blockly.FieldTextInput("100"), "time");
      this.setColour(300);
   this.setTooltip("");
   this.setHelpUrl("https://create.dronaaviation.com/assets/downloads/PlutoBlocks/Guide%20to%20Pluto%20Blocks%20updated.pdf");
    }
};

  Blockly.defineBlocksWithJsonArray([{
    "type": "pluto_on_stop_loop",
    "message0": "on start %1 %2",

    "args0": [
      {
        "type": "input_dummy",
        "align": "RIGHT"
      },
      {
        "type": "input_statement",
        "name": "NAME",
        "align": "CENTRE"
      }
    ],
    "colour": 300,
    "tooltip": "",
    "helpUrl": "https://create.dronaaviation.com/assets/downloads/PlutoBlocks/Guide%20to%20Pluto%20Blocks%20updated.pdf"
  }]);
