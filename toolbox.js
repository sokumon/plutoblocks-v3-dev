// this is a pure JSON toolbox definition. It can be used to define a custom toolbox
const toolbox = 
{
  "kind": "categoryToolbox",
  "contents": [
    {
      "kind": "category",
      "name": "Starter",
      "contents": [
        {
          "kind": "block",
          "blockxml": "<block type=\"pluto_on_start_loop\" deletable=\"false\" x=\"0\" y=\"0\"></block>",
          "type": "pluto_on_start_loop"
        },{
          "kind": "block",
          "type": "pluto_loop"
        },{
          "kind": "block",
          "blockxml": "<block type=\"pluto_on_stop_loop\" deletable=\"false\" x=\"0\" y=\"400\"></block>",
          "type": "pluto_on_stop_loop"
        }
      ],
      "colour": "#5b80a5",
      
    },
    {
      "kind": "category",
      "name": "LED",
      "contents": [
        {
          "kind": "block",
          "type": "red_led"
        },
        {
          "kind": "block",
          "type": "green_led"
        },
        {
          "kind": "block",
          "type": "blue_led"
        }
      ],
      "colour":123
    },{
        "kind": "category",
        "name": "GPIO",
        "colour":265,
        "contents": [{
            "kind": "block",
            "type": "pin_10"
        },{
            "kind": "block",
            "type": "gpio_read"
        },{
            "kind": "block",
            "type": "adc_read"
        }]
    },{
        "kind": "category",
        "name": "PWM",
        "colour":100,
        "contents": [{
            "kind": "block",
            "type":"set_servo",
            "inputs":{
                "pwm":{
                  "shadow":{
                    "type":"math_number",
                    "field":{
                      "NUM":12
                    }
                  }
                }
            },
        }]
    },{
        "kind": "category",
        "name": "Command",
        "colour":222,
        "contents": [{
            "kind":"block",
            "type":"take_off"
        },{
            "kind":"block",
            "type":"land"
        },{
            "kind":"block",
            "type":"flip"
        },{
            "kind":"block",
            "type":"arm"
        },{
            "kind":"block",
            "type":"disarm"
        },]
    },{
        "kind": "category",
        "name": "Motor",
        "colour":350,
        "contents": [{
            "kind": "block",
            "type":"set_m5"
        },{
            "kind": "block",
            "type":"set_m6"
        },{
            "kind": "block",
            "type":"set_m7"
        },{
            "kind": "block",
            "type":"set_m8"
        }]
    },{
        "kind": "category",
        "name": "Timer",
        "contents": [{
            "kind": "block",
            "type":"set_timer",
            "extensions": ["set_timer_on_start_check"]
          },{
            "kind": "block",
            "type":"reset_timer"
          },{
            "kind": "block",
            "type":"timer_check"
          }]
    },{
        "kind": "category",
        "name": "Remote Control",
        "colour":270,
        "contents": [{
            "kind": "block",
            "type":"rc_roll"
        },{
            "kind": "block",
            "type":"rc_pitch"
        },{
            "kind": "block",
            "type":"rc_yaw"
        },{
            "kind": "block",
            "type":"rc_throttle"
        }]
    },{
        "kind": "category",
        "name": "Sensor",
        "contents": [{
            "kind": "block",
            "type":"get_acc"
        },{
            "kind": "block",
            "type":"get_gyroscope"
        },{
            "kind": "block",
            "type":"get_mag"
        },{
            "kind": "block",
            "type":"get_baro"
        }]
    },{
        "kind": "category",
        "name":"Current State",
        "colour":270,
        "contents":[{
            "kind": "block",
            "type":"current_roll"
        },{
            "kind": "block",
            "type":"current_pitch"
        },{
            "kind": "block",
            "type":"current_yaw"

        },{
            "kind": "block",
            "type":"current_height"
        },{
            "kind": "block",
            "type":"app_heading"
        }]
    },{
        "kind": "category",
        "name" : "Desired State",
        "colour":250,
        "contents":[{
            "kind": "block",
            "type":"desired_roll"
        },{
            "kind": "block",
            "type":"desired_pitch"
        },{
            "kind": "block",
            "type":"desired_yaw"
        },{
            "kind": "block",
            "type":"desired_height"
        }] 
    },{
        "kind": "category",
        "name":"Flight Mode",
        "colour":150,
        "contents":[{
            "kind": "block",
            "type":"is_headfree"
        },{
            "kind": "block",
            "type":"is_throttle_mode"
        }]
    },{
        "kind": "category",
        "name":"Flight Status",
        "colour":250,
        "contents":[{
            "kind": "block",
            "type":"is_armed"
        },{
            "kind": "block",
            "type":"is_ok_to_arm"
        },{
            "kind": "block",
            "type":"is_signal_loss"
        },{
            "kind":"block",
            "type":"is_crashed"
        },{
            "kind":"block",
            "type":"is_inflight_low_battery"
        },{
            "kind":"block",
            "type":"is_low_battery"
        }]
    },{
        "kind": "category",
        "name":"Console",
        "contents":[{
            "kind": "block",
            "type": "pluto_monitor_print"
        },{
            "kind": "block",
            "type": "pluto_monitor_print_var"
        }],
        "colour":100
    },{
        "kind": "category",
        "name":"Logic",
        "contents":[{
            "kind": "block",
            "type": "controls_if"
        },{
            "kind": "block",
            "type": "logic_compare"
        },{
            "kind": "block",
            "type": "logic_operation"
        },{
            "kind":"block",
            "type":"logic_negate"
        },{
            "kind":"block",
            "type":"logic_boolean"    
        },{
            "kind":"block",
            "type":"logic_null",
        },{
            "kind":"block",
            "type":"logic_ternary"
        }],
        "colour":200
    },{
        "kind": "category",
        "name":"Loops",
        "colour":378,
        "contents":[{
            "kind": "block",
            "type": "controls_repeat_ext",
        },{
            "kind": "block",
            "type": "controls_whileUntil",
        },{
            "kind": "block",
            "type":"controls_for"
        },{
            "kind": "block",
            "type":"controls_flow_statements"
        }]
    },{
        "kind": "category",
        "name":"Math",
        "colour":412,
        "contents":[{
            "kind": "block",
            "type": "math_number",
        },{
            "kind": "block",
            "type": "math_arithmetic",
        },{
            "kind": "block",
            "type": "math_single",
        },{
            "kind": "block",
            "type": "math_trig",
        },{
            "kind": "block",
            "type": "math_constant",
        },{
            "kind": "block",
            "type": "math_number_property",    
        },{
            "kind": "block",
            "type": "math_round",
        },{
            "kind": "block",
            "type": "math_modulo"               
        },{
            "kind": "block",
            "type":"math_constrain"
        },{
            "kind": "block",
            "type": "math_random_int",
        },{
            "kind": "block",
            "type": "math_random_float",
        }
        ]      
    },{
        "kind": "category",
        "name":"Variables",
        "colour":330,
        "contents":[{
            "kind": "label",
            "text": "Integer Variables"
          },{
            "kind": "block",
            "type": "variables_get",
        },{
            "kind": "block",
            "type": "variables_set",

        },{
            "kind": "label",
            "text": "Float Variables"
        },{
            "kind": "block",
            "type": "variables_set_float",
        },{
            "kind": "block",
            "type": "variables_get_float",
        },{
            "kind": "label",
            "text": "Boolean Variables"
        },{
            "kind": "block",
            "type": "variables_set_bool",
        },{
            "kind": "block",
            "type": "variables_get_bool",
        }]
    },{
        "kind": 'category',
        "id": 'catFunctions',
        "colour": '290',
        "custom": 'PROCEDURE',
        "name": 'Functions',
      }
  ]
}