// Description: This file is used to inject the Blockly workspace into the HTML page.

// class CustomCategory extends Blockly.ToolboxCategory {
//     /**
//      * Constructor for a custom category.
//      * @override
//      */
//     constructor(categoryDef, toolbox, opt_parent) {
//       super(categoryDef, toolbox, opt_parent);
//     }
// }
// Blockly.registry.register(
//     Blockly.registry.Type.TOOLBOX_ITEM,
//     Blockly.ToolboxCategory.registrationName,
//     CustomCategory, true);

// define the 
Blockly.setLocale('en');
const options_for_workspace = {grid:
  {spacing: 25,
   length: 3,
   colour: '#111111',
   snap: true},
media: './node_modules/blockly/media/',
toolbox: toolbox,
zoom:
   {controls: true,
    wheel: true}
};
const workspace = Blockly.inject('blocklyDiv',{toolbox: toolbox});
// registeing callbacks for the buttons like create timer and create variable
workspace.registerButtonCallback("myFirstButtonPressed", function(button) {
      Blockly.Variables.createVariableButtonHandler(button.getTargetWorkspace(), null, 'int');
   }
)

// Blockly.serialization.workspaces.load(state, workspace);
// define the new toolbox with categories
