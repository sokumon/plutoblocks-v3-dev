Blockly.defineBlocksWithJsonArray([{
    "type": "red_led",
    "message0": "RED %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "ON",
            "ON"
          ],
          [
            "OFF",
            "OFF"
          ],
          [
            "TOGGLE",
            "TOGGLE"
          ]
        ]
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "green_led",
    "message0": "GREEN %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "ON",
            "ON"
          ],
          [
            "OFF",
            "OFF"
          ],
          [
            "TOGGLE",
            "TOGGLE"
          ]
        ]
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "blue_led",
    "message0": "BLUE %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "ON",
            "ON"
          ],
          [
            "OFF",
            "OFF"
          ],
          [
            "TOGGLE",
            "TOGGLE"
          ]
        ]
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "pin_10",
    "message0": "Set Pin %1 to %2",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "2",
            "2"
          ],
          [
            "3",
            "3"
          ],
          [
            "4",
            "4"
          ],
          [
            "5",
            "5"
          ],
          [
            "6",
            "6"
          ],
          [
            "7",
            "7"
          ],
          [
            "8",
            "8"
          ],
          [
            "9",
            "9"
          ],
          [
            "10",
            "10"
          ],
          [
            "11",
            "11"
          ],
          [
            "12",
            "12"
          ],
          [
            "13",
            "13"
          ],
          [
            "14",
            "14"
          ],
          [
            "15",
            "15"
          ],
          [
            "16",
            "16"
          ],
          [
            "17",
            "17"
          ],
          [
            "18",
            "18"
          ],
          [
            "19",
            "19"
          ]
        ]
      },
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "LOW",
            "LOW"
          ],
          [
            "HIGH",
            "HIGH"
          ],
          [
            "TOGGLE",
            "TOGGLE"
          ]
        ]
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 65,
    "tooltip": "",
    "helpUrl": ""
  },{
    "type": "gpio_read",
    "message0": "Read %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "pin_no",
        "options": [
          [
            "2",
            "2"
          ],
          [
            "3",
            "3"
          ],
          [
            "6",
            "6"
          ],
          [
            "7",
            "7"
          ],
          [
            "8",
            "8"
          ],
          [
            "9",
            "9"
          ],
          [
            "10",
            "10"
          ],
          [
            "11",
            "11"
          ],
          [
            "12",
            "12"
          ],
          [
            "13",
            "13"
          ],
          [
            "14",
            "14"
          ],
          [
            "15",
            "15"
          ],
          [
            "16",
            "16"
          ],
          [
            "17",
            "17"
          ],
          [
            "18",
            "18"
          ],
          [
            "19",
            "19"
          ]
        ]
      }
    ],
    "output": null,
    "colour": 75,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "adc_read",
    "message0": "Read ADC Pin %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "8",
            "8"
          ],
          [
            "13",
            "13"
          ],
          [
            "14",
            "14"
          ],
          [
            "15",
            "15"
          ],
          [
            "16",
            "16"
          ],
          [
            "17",
            "17"
          ],
          [
            "18",
            "18"
          ],
          [
            "19",
            "19"
          ]
        ]
      }
    ],
    "output": null,
    "colour": 65,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_servo",
    "message0": "Set PWM pin %1 to %2",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "2",
            "2"
          ],
          [
            "3",
            "3"
          ],
          [
            "4",
            "4"
          ],
          [
            "5",
            "5"
          ],
          [
            "8",
            "8"
          ],
          [
            "9",
            "9"
          ],
          [
            "10",
            "10"
          ],
          [
            "13",
            "13"
          ],
          [
            "15",
            "15"
          ],
          [
            "18",
            "18"
          ],
          [
            "19",
            "19"
          ]
        ]
      },
      {
        "type": "input_value",
        "name": "pwm",
        "check": "Number"
      }
    ],
    "inputsInline": true,
    "inputs":{
      "pwm": {
        "A": {
          "shadow": {
            "type": "math_number",
            "fields": {
              "NUM": 1
            }
          }
        }
      }
    },
    "previousStatement": null,
    "nextStatement": null,
    "colour": 120,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "take_off",
    "message0": "take off %1 cm",
    "args0": [
      {
        "type": "field_number",
        "name": "NAME",
        "value": 100,
        "min": 100,
        "max": 250
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 20,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "land",
    "message0": "land %1 speed",
    "args0": [
      {
        "type": "field_number",
        "name": "NAME",
        "value": 105,
        "min": 0,
        "max": 255
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 20,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "flip",
    "message0": "flip",
    "previousStatement": null,
    "nextStatement": null,
    "colour": 20,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "arm",
    "message0": "arm",
    "previousStatement": null,
    "nextStatement": null,
    "colour": 20,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "disarm",
    "message0": "disarm",
    "previousStatement": null,
    "nextStatement": null,
    "colour": 20,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_m5",
    "message0": "Set M5 to %1",
    "args0": [
      {
        "type": "input_value",
        "name": "pwm",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 120,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_m6",
    "message0": "Set M6 to %1",
    "args0": [
      {
        "type": "input_value",
        "name": "pwm",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 120,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_m7",
    "message0": "Set M7 to %1",
    "args0": [
      {
        "type": "input_value",
        "name": "pwm",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 120,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_m8",
    "message0": "Set M8 to %1",
    "args0": [
      {
        "type": "input_value",
        "name": "pwm",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 120,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "rc_roll",
    "message0": "RC Roll",
    "output": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "rc_pitch",
    "message0": "RC Pitch",
    "output": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "rc_yaw",
    "message0": "RC Yaw",
    "output": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "rc_throttle",
    "message0": "RC Throttle",
    "output": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_pitch",
    "message0": "Set pitch %1",
    "args0": [
      {
        "type": "input_value",
        "name": "pitch_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_roll",
    "message0": "Set roll %1",
    "args0": [
      {
        "type": "input_value",
        "name": "roll_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_yaw",
    "message0": "Set yaw %1",
    "args0": [
      {
        "type": "input_value",
        "name": "yaw_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "set_throttle",
    "message0": "Set throttle %1",
    "args0": [
      {
        "type": "input_value",
        "name": "throttle_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "get_acc",
    "message0": "Acceleration (cm/s2) %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "x",
            "x"
          ],
          [
            "y",
            "y"
          ],
          [
            "z",
            "z"
          ],
          [
            "strength",
            "strength"
          ]
        ]
      }
    ],
    "output": null,
    "colour": 240,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "get_gyroscope",
    "message0": "Gyroscope (DeciDegree/s) %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "x",
            "x"
          ],
          [
            "y",
            "y"
          ],
          [
            "z",
            "z"
          ]
        ]
      }
    ],
    "output": null,
    "colour": 240,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "get_mag",
    "message0": "Magnetometer(microTesla) %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "x",
            "x"
          ],
          [
            "y",
            "y"
          ],
          [
            "z",
            "z"
          ]
        ]
      }
    ],
    "output": null,
    "colour": 240,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "get_baro",
    "message0": "Barometer %1",
    "args0": [
      {
        "type": "field_dropdown",
        "name": "NAME",
        "options": [
          [
            "pressure(100*)millibar",
            "pressure(100*millibar)"
          ],
          [
            "temperature(100*°C)",
            "temperature(100*°C)"
          ]
        ]
      }
    ],
    "output": null,
    "colour": 240,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "current_roll",
    "message0": "Current Roll (Degrees)",
    "output": null,
    "colour": 270,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "current_pitch",
    "message0": "Current Pitch (Degrees)",
    "output": null,
    "colour": 270,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "current_yaw",
    "message0": "Current Yaw (Degrees)",
    "output": null,
    "colour": 270,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "current_height",
    "message0": "Current height (Degrees)",
    "output": null,
    "colour": 270,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "app_heading",
    "message0": "Get App Heading",
    "output": null,
    "colour": 270,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "desired_roll",
    "message0": "Desired roll(Degrees) %1",
    "args0": [
      {
        "type": "input_value",
        "name": "roll_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "desired_pitch",
    "message0": "Desired Pitch(Degrees) %1",
    "args0": [
      {
        "type": "input_value",
        "name": "pitch_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "desired_yaw",
    "message0": "Desired Yaw(Degrees) %1",
    "args0": [
      {
        "type": "input_value",
        "name": "yaw_value",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "desired_height",
    "message0": "Desired Height (cm) %1",
    "args0": [
      {
        "type": "input_value",
        "name": "height",
        "check": "Number"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_headfree",
    "message0": "is headfree mode",
    "output": null,
    "colour": 90,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_throttle_mode",
    "message0": "is throttle mode",
    "output": null,
    "colour": 90,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_armed",
    "message0": "is armed",
    "output": null,
    "colour": 45,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_ok_to_arm",
    "message0": "ok to arm",
    "output": null,
    "colour": 45,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_signal_loss",
    "message0": "is Signal lost",
    "output": null,
    "colour": 45,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_crashed",
    "message0": "is crashed",
    "output": null,
    "colour": 45,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_inflight_low_battery",
    "message0": "is inflight low battery",
    "output": null,
    "colour": 45,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "is_low_battery",
    "message0": "is low battery",
    "output": null,
    "colour": 45,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "servo_pwm",
    "message0": "%1",
    "args0": [
      {
        "type": "field_number",
        "name": "pwm",
        "value": 1000,
        "min": 1000,
        "max": 2000
      }
    ],
    "output": null,
    "colour": 120,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "field_angle_roll",
    "message0": "%1",
    "args0": [
      {
        "type": "field_angle",
        "name": "angle_field",
        "angle": 0
      }
    ],
    "output": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "rc_value",
    "message0": "%1",
    "args0": [
      {
        "type": "field_number",
        "name": "RcValue",
        "value": 1000,
        "min": 1000,
        "max": 2000
      }
    ],
    "output": null,
    "colour": 330,
    "tooltip": "",
    "helpUrl": ""
  },
  {
    "type": "shadow_number",
    "message0": "%1",
    "args0": [
      {
        "type": "field_number",
        "name": "NUM",
        "value": 0
      }
    ],
    "output": null,
    "colour": 165,
    "tooltip": "",
    "helpUrl": ""
  },{
    "type": "pluto_monitor_print",
    "message0": "print %1",
    "args0": [
      {
        "type": "field_input",
        "name": "TEXT",
        "text": "Hello World"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  },{
    "type": "pluto_monitor_print_var",
    "message0": "print %1",
    "args0": [
      {
        "type": "input_value",
        "name": "NAME",
        "align": "CENTRE"
      }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": 230,
    "tooltip": "",
    "helpUrl": ""
  }]);

  Blockly.Blocks.pluto_interval = {};
  
  Blockly.Blocks.timer_slider = {
      init: function() {
          this.appendDummyInput().appendField(new Blockly.FieldNumber(1e3, 0, 5e3), "TIME");
          this.setOutput(!0, "Number");
          this.setColour(120);
          this.setTooltip("");
          this.setHelpUrl("");
      }
  };
  
  Blockly.Blocks.set_timer = {
      init: function() {
          this.appendDummyInput().appendField("set").appendField(new Blockly.FieldVariable("T1", "", [ "Interval" ], "Interval"), "VAR");
          this.appendValueInput("TIME").setCheck("Number").appendField("time (ms)");
          this.appendValueInput("REPEAT").setCheck("Boolean").appendField("repeat");
          this.setPreviousStatement(!0, null);
          this.setNextStatement(!0, null);
          this.setColour(50);
          this.setTooltip("");
          this.setHelpUrl("");
          // this.setExtension("set_timer_on_start_check");
      }
  };
  
  Blockly.Blocks.reset_timer = {
      init: function() {
          this.appendDummyInput().appendField("reset").appendField(new Blockly.FieldVariable("T1", "", [ "Interval" ], "Interval"), "VAR");
          this.setPreviousStatement(!0, null);
          this.setNextStatement(!0, null);
          this.setColour(50);
          this.setTooltip("");
          this.setHelpUrl("");
      }
  };
  
  Blockly.Blocks.timer_check = {
      init: function() {
          this.appendDummyInput().appendField("check").appendField(new Blockly.FieldVariable("T1", "", [ "Interval" ], "Interval"), "VAR");
          this.setOutput(!0, "Boolean");
          this.setColour(50);
          this.setTooltip("");
          this.setHelpUrl("");
      }
  };

// Varibles
Blockly.Blocks.variables_get = {
  init: function() {
      this.appendDummyInput().appendField(new Blockly.FieldVariable("item", "", [ "", "int32_t" ], ""), "VAR");
      this.setOutput(!0, "Number");
      this.setColour(330);
      this.setTooltip("");
      this.setHelpUrl("");
  }
};

Blockly.Blocks.variables_set = {
  init: function() {
      this.appendValueInput("VALUE").setCheck(null).appendField("set").appendField(new Blockly.FieldVariable("item", "", [ "", "int32_t" ], ""), "VAR").appendField("to");
      this.setPreviousStatement(!0, null);
      this.setNextStatement(!0, null);
      this.setColour(330);
      this.setTooltip("%{BKY_VARIABLES_SET_TOOLTIP}");
      this.setHelpUrl("%{BKY_VARIABLES_SET_HELPURL}");
  }
};

Blockly.Blocks.variables_set_bool = {
  init: function() {
      this.appendValueInput("VALUE").setCheck(null).appendField("set").appendField(new Blockly.FieldVariable("item", "", [ "bool" ], "bool"), "VAR").appendField("to");
      this.setPreviousStatement(!0, null);
      this.setNextStatement(!0, null);
      this.setColour(330);
      this.setTooltip("%{BKY_VARIABLES_SET_TOOLTIP}");
      this.setHelpUrl("%{BKY_VARIABLES_SET_HELPURL}");
  }
};

Blockly.Blocks.variables_get_bool = {
  init: function() {
      this.appendDummyInput().appendField(new Blockly.FieldVariable("item", "", [ "bool" ], "bool"), "VAR");
      this.setOutput(!0, "Boolean");
      this.setColour(330);
      this.setTooltip("");
      this.setHelpUrl("");
  }
};

Blockly.Blocks.variables_set_float = {
  init: function() {
      this.appendValueInput("VALUE").setCheck(null).appendField("set").appendField(new Blockly.FieldVariable("item", "", [ "float" ], "float"), "VAR").appendField("to");
      this.setPreviousStatement(!0, null);
      this.setNextStatement(!0, null);
      this.setColour(330);
      this.setTooltip("%{BKY_VARIABLES_SET_TOOLTIP}");
      this.setHelpUrl("%{BKY_VARIABLES_SET_HELPURL}");
  }
};

Blockly.Blocks.variables_get_float = {
  init: function() {
      this.appendDummyInput().appendField(new Blockly.FieldVariable("item", "", [ "float" ], "float"), "VAR");
      this.setOutput(!0, "Number");
      this.setColour(330);
      this.setTooltip("");
      this.setHelpUrl("");
  }
};